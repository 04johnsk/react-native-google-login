// GoogleLoginManager
#import "RCTBridgeModule.h"
#import <GooglePlus/GooglePlus.h>

@interface GoogleLoginManager : NSObject <RCTBridgeModule, GPPSignInDelegate>
@end