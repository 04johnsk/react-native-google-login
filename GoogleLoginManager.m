//
//  GoogleLoginManager.m
//  googlelogin
//
//  Created by Kyle Johnson on 09/07/2015.
//  Copyright (c) 2015 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GoogleLoginManager.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>



// GoogleManager.m
@implementation GoogleLoginManager
static NSString * const kClientId = @"337815820207-dsj4duog34e70a9v14nelo2gkbsfoooi.apps.googleusercontent.com";
static NSString * const kScope = @"email";

RCTResponseSenderBlock callback;

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(login:(RCTResponseSenderBlock)c)
{
  callback = c;
  GPPSignIn *signIn = [GPPSignIn sharedInstance];
  signIn.clientID = kClientId;
  signIn.shouldFetchGoogleUserID = YES;
  signIn.scopes = @[ kScope ];
  signIn.delegate = self;
  [signIn signOut];
  [signIn authenticate];
}

RCT_EXPORT_METHOD(logout:(RCTResponseSenderBlock)callback)
{
  GPPSignIn *signIn = [GPPSignIn sharedInstance];
  [signIn signOut];
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
  NSLog(@"Received error %@ and auth object %@",error, auth);
  callback(@[[NSNull null], auth.parameters]);
}



@end